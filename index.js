// Load the expressjs module into our application and save it in variable called express
const express = require("express");
const port = 4000;

// app is our server
// create an application that uses express and store it as app
const app = express();

// Middleware (request handlers)
// express.json() is a method which allows us to handle the streaming of data and automatically parse the incoming JSON from our req.body
app.use(express.json());

// mock data

let users = [
    {
        username: "TStark3000",
        email: "starindustries@mail.com",
        password: "notPeterParker"
    },
    {
        username: "ThorThunder",
        email: "thorStrongestAvenger@mail.com",
        password: "iLoveStormBreaker"
    }
]

let items = [
    {
        name: "Mjolnir",
        price: 50000,
        isActive: true
    },
    {
        name: "Vibranium Shield",
        price: 70000,
        isActive: true
    }
]

app.get("/", (request, response) => {
    response.send("Hello from my first ExpressJSAPI");
    }
)

app.get("/greeting", (request, response) => {
    response.send("Hello from my Batch230-Ferrer");
    }
)

// retrievel of users in mock database
app.get("/users", (req, res) => {
    res.send(users);
}), 

app.post("/users" , (request, response) => {
    
    let newUser = {
        username: request.body.username,
        email: request.body.email,
        password: request.body.password
    }

    users.push(newUser);
    console.log(users);

    response.send(users)
})

app.put("/users/:index", (req,res) => {
    console.log(req.body);

    console.log(req.params); // index: 1

    let index = parseInt(req.params.index); // '1' >> 1
    users[index].password = req.body.password;
    res.send(users[index]);
})

// Delete the last document in an array
app.delete('/users', (req, res) => {
    users.pop()
    res.send(users);

let items = [
    {
        name: "Mjolnir",
        price: 50000,
        isActive: true
    },
    {
        name: "Vibranium Shield",
        price: 70000,
        isActive: true
    }
]

})
// ACtivity
/*
    >> Create a new collection in Postman called s34-activity
    >> Save the Postman collection in your s34 folder
*/
// [GET]
// >> Create a new route to get and send items array in the client (GET ALL ITEMS)
app.get("/items", (req, res) => {
    res.send(items);
}), 

// [POST]
// >> Create a new route to create and add a new item object in the items array (CREATE ITEM)
    // >> send the updated items array in the client
    // >> check if the post method route for our users for reference
    app.post("/items" , (request, response) => {
    
        let createItem = {
            
            name: request.body.name,
            price: request.body.price,
            isActive: request.body.isActive
            
        }
    
        items.push(createItem);
        console.log(items);
    
        response.send(items)
    })


// [PUT]
// Create a new route which can update the price of a single item in the array (UPDATE ITEM)
/*
    >> Pass the index number of the item that you want to update in the request params ( include an index number to the URL)
    >> add the price update in the request body
    >> send the updated item to the client
*/
app.put("/items/:index", (req,res) => {
    console.log(req.body);

    console.log(req.params); 

    let index = parseInt(req.params.index);
    items[index].price = req.body.price;
    res.send(items[index]);
})



app.listen(port, () => console.log(`Server is now running at port ${port}`));

